#!/usr/bin/env python
# Rui Zhang 4.2020
# rui.zhang@cern.ch

import pickle
from os import environ, path

def setupMLflow(name):
    import mlflow.keras
    mlflow.keras.autolog()
    mlflow_home = f"{environ.get('SHAREDIR')}/{name}/mlflow"
    mlflow.tracking.set_tracking_uri(mlflow_home)
    mlflow.set_experiment(name)
    if not path.exists('/.tmp_MLFLOW_TRACKING_URI'):
        with open('/.tmp_MLFLOW_TRACKING_URI', 'w') as text_file:
            text_file.write(mlflow_home)

    return mlflow_home


def RetrieveOpt(opt_name, library):
    assert(path.exists(opt_name)), opt_name
    optimizer = None
    if library== 'nevergrad':
        with open(opt_name, 'rb') as fp:
            optimizer = pickle.load(fp)
    elif library == 'skopt':
        from skopt import load
        optimizer = load(opt_name)
    else:
        assert(False), f'{library} is not supported'

    return optimizer

def RetrievePoints(points_name):
    assert(path.exists(points_name)), points_name
    with open(points_name, 'rb') as fp:
        stored_points = pickle.load(fp)
        print('Read opt points from ', points_name)
        print(stored_points)
    return stored_points

def SaveObjPoints(points_name, stored_points, objective_points):
    with open(points_name, 'wb') as fp:
        pickle.dump([stored_points, objective_points], fp)
        print('Save obj points to ', points_name)
