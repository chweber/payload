#!/usr/bin/env python
# Rui Zhang 4.2020
# rui.zhang@cern.ch

import numpy as np
import uproot
import pandas
from sklearn.model_selection import train_test_split
from keras import models
from keras import layers
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from sklearn.metrics import roc_curve
from skopt import load
import pickle
from os import path, environ


def objective(train_x_S, train_x_B, train_y_S, train_y_B, test_x_S, test_x_B, test_y_S, test_y_B, *space_point):
    num_hidden_layers, lr, mom = tuple(space_point) if space_point else (3, 0.01, 0.8)

    train_x = np.concatenate((train_x_S, train_x_B))
    train_y = np.concatenate((train_y_S, train_y_B))
    test_x = np.concatenate((test_x_S, test_x_B))
    test_y = np.concatenate((test_y_S, test_y_B))

    model = Sequential()
    model.add(layers.Dense(4, activation = 'relu', input_shape=(train_x.shape[1], )))
    for i in range(num_hidden_layers):
        model.add(layers.Dense(4, activation = 'elu'))
    model.add(layers.Dense(1, activation = 'sigmoid'))
    model.summary()

    sgd = SGD(lr=lr, momentum=mom)
    model.compile(optimizer = sgd, loss = 'binary_crossentropy', metrics=['accuracy'])

    results = model.fit(train_x, train_y, epochs= 10, batch_size = 512, validation_data = (test_x, test_y), verbose=1)
    return results.history['val_loss'][-1]

def readNtuple():

    tree_S = uproot.open('dataset/tmva_class_example.root')['TreeS']
    tree_B = uproot.open('dataset/tmva_class_example.root')['TreeB']

    events_S = tree_S.pandas.df(['var1', 'var2', 'var3', 'var4']).values
    events_B = tree_B.pandas.df(['var1', 'var2', 'var3', 'var4']).values

    train_x_S, test_x_S = train_test_split(events_S, train_size=0.6, test_size=0.4)
    train_x_B, test_x_B = train_test_split(events_B, train_size=0.6, test_size=0.4)

    train_y_S = np.reshape([1 for x in range(len(train_x_S))], (len(train_x_S), 1))
    test_y_S = np.reshape([1 for x in range(len(test_x_S))], (len(test_x_S), 1))
    train_y_B = np.reshape([0 for x in range(len(train_x_B))], (len(train_x_B), 1))
    test_y_B = np.reshape([0 for x in range(len(test_x_B))], (len(test_x_B), 1))

    return train_x_S, train_x_B, train_y_S, train_y_B, test_x_S, test_x_B, test_y_S, test_y_B
