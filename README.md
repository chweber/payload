# Collection of payloads for Evaluation container jobs
Each job is supposed to run one Hyperparameter point created by upstream iDDS component.

# 1. btagging
## 1.1. Run with a [docker](https://www.docker.com) container
- Run a docker container interactively choosing one of the following ways:
  - From gitlab image (recommended)
  - From local image
```
docker run --rm -it gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:mlflow bash
```
```
docker build -t $USER/test-end -f docker/mlflow/Dockerfile .
docker run -it $USER/test-end /bin/bash
```

- Run training _inside_ the container environment by the following commands
  - Add $PYTHONPATH
  - Download input data (1.4GB) ([origin](https://cernbox.cern.ch/index.php/s/rtU9DSOCBhACZaL/download))
  - Run trainining

```
export PYTHONPATH=/ATLASMLHPO/payload/btagging/DL1_framework/Training:$PYTHONPATH
```
```
cd /ATLASMLHPO/payload/btagging/; curl -sSL curl -sSL https://cernbox.cern.ch/index.php/s/rtU9DSOCBhACZaL/download  | tar -xzvf -
```
```
python /ATLASMLHPO/payload/btagging/train.py \
    --configs /ATLASMLHPO/payload/btagging/configs/myconfig.json \
    --trainingfile /ATLASMLHPO/payload/btagging/dataset/MC16d_hybrid-training_sample-NN.h5 \
    --validationfile /ATLASMLHPO/payload/btagging/dataset/MC16d_hybrid_odd_100_PFlow-validation.h5 \
    --validation_config /ATLASMLHPO/payload/btagging/DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json \
    --variables /ATLASMLHPO/payload/btagging/DL1_framework/Training/configs/DL1r_Variables.json \
    --outputfile output.json --large_file \
    --epochs 1
```

# 2. CaloImageDNN
## 2.1 Run locally with a [docker](https://www.docker.com) container
- Run a docker container interactively choosing one of the following ways:
  - From gitlab image (recommended)
  - From local image
```
docker run --rm -it gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:mlflow bash
```
```
docker build -t $USER/test-end -f docker/mlflow/Dockerfile .
docker run -it $USER/test-end /bin/bash
```

- Run training _inside_ the container environment by the following commands
  - Add $PYTHONPATH
  - Download input data (27MB)
  - Run trainining
```
export PYTHONPATH=/ATLASMLHPO/payload/CaloImageDNN/deepcalo:$PYTHONPATH
```
```
cd /ATLASMLHPO/payload/CaloImageDNN/; curl -sSL https://cernbox.cern.ch/index.php/s/HfHYEsmJNWiefu3/download | tar -xzvf -; 
```
```
source /ATLASMLHPO/payload/CaloImageDNN/run.sh
```
## 2.2. Submit to Grid

Take the `CaloImageDNN` as an example, the following command allows a Grid job submission
(on lxplus)
```
setupATLAS
lsetup panda
prun --containerImage \
docker://gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:mlflow \
 --exec 'export PYTHONPATH=/ATLASMLHPO/payload/CaloImageDNN/deepcalo:/ATLASMLHPO/payload/btagging/DL1_framework/Training:$PYTHONPATH; \
cd /ATLASMLHPO/payload/CaloImageDNN/; curl -sSL https://cernbox.cern.ch/index.php/s/HfHYEsmJNWiefu3/download | tar -xzvf -; \
source run.sh' \
--noBuild \
--cmtConfig nvidia-gpu \
--site ANALY_MANC_GPU_TEST \
--outDS user.zhangr.test403
```

# 3. SkoptExample
This is an example to run `ask-and-tell` interface of `skopt`.
Refer to [`Orchestrator` README](https://gitlab.cern.ch/zhangruihpc/orchestrator).

# 4. CaloImageDNN with `mlflow`
The [`mlflow`](https://www.mlflow.org/docs/latest/index.html) library is an open source platform for managing the end-to-end machine learning lifecycle.
We will build a `mlmlflow` container for it.

<details><summary>Code adapting</summary>
<p>

#### For `Keras` script, add these lines to training script.

```
import mlflow.keras
mlflow.keras.autolog()
mlflow.set_experiment("my-experiment")
```
`my-experiment` is customised name of your experiment.
</p>
</details>


## 4.1 Run container locally
- Choose one of the following ways:
  - From gitlab image (recommended)
  - From local image
```
docker run --rm -it -p 5000:5000 gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:mlflow bash
```
```
docker build -t $USER/test-end2 -f docker/mlflow/Dockerfile .
docker run -it -p 5000:5000 $USER/test-end2 /bin/bash
```

## 4.2 Run training
(in container)
```
export PYTHONPATH=/ATLASMLHPO/payload/CaloImageDNN/deepcalo:$PYTHONPATH
cd /ATLASMLHPO/payload/CaloImageDNN/; curl -sSL https://cernbox.cern.ch/index.php/s/HfHYEsmJNWiefu3/download | tar -xzvf -; 
source /ATLASMLHPO/payload/CaloImageDNN/run.sh
```

## 4.3 View training results
(in container)
```
mlflowUI.sh
```
Then in localhost machine, browse:

http://127.0.0.1:5000

The name of the experiment (`my-experiment`) is listed on the left.

# 5. A Keras training with `mlflow` and `skopt`

This is an example to run `ask-and-tell` interface of `skopt` and record results by `mlflow`.
Refer to [`Orchestrator` README](https://gitlab.cern.ch/zhangruihpc/orchestrator).


## 5.0. Changes to the training code to use `mlflow` 
<details><summary>Code adapting</summary>
<p>

#### For `Keras` script, add these lines to training script.

```
import mlflow.keras
mlflow.keras.autolog()
mlflow.set_experiment("my-experiment")
```
`my-experiment` is customised name of your experiment.
</p>
</details>
