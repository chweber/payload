import os
import copy
import numpy as np
import argparse
import joblib
import deepcalo as dpcal
from sklearn.preprocessing import RobustScaler, QuantileTransformer
import json

from mlflowautolog import mlflow_keras_autolog
mlflow_keras_autolog("my-keras")

# ==============================================================================
# Argument parsing
# ==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
parser.add_argument('--data_path', help='Path to data. Can be relative.', default=None, type=str)
parser.add_argument('--exp_dir', help='Directory of experiment, e.g. "my_exp/".', default='./', type=str)
parser.add_argument('--n_train', help='How many data points to load from the training set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_val', help='How many data points to load from the validation set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_test', help='How many data points to load from the test set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--save_figs', help='Save figures.', default=True, type=dpcal.utils.str2bool)
parser.add_argument('--rm_bad_reco', help='Removes all points that are badly reconstructed by the current energy calibration.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--zee_only', help='Uses only Z->ee data.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--lh_cut_name', help='Which, if any, likehood cut to apply. Should be the name of the variable that should be True for that point to not be masked out. E.g., for electrons, use "p_LHLoose", while for photons, use "p_photonIsLooseEM" (or "Medium" or "Tight"). None (default) will not apply any likehood cut.', default=None, type=str)
parser.add_argument('--mask_path', help='Path to a mask. Effectively replaces n_points given to the loading function.', default=None, type=str)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
parser.add_argument('--wandb', help='Run wandb monitor.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('-i', '--input_json', help='Input json file.', default='input.json', type=str)
parser.add_argument('-o', '--output_json', help='Output json file.', default='output.json', type=str)
args = parser.parse_args()

gpu_ids = args.gpu
data_path = args.data_path
exp_dir = args.exp_dir
_n_train = int(args.n_train)
_n_val = int(args.n_val)
_n_test = int(args.n_test)
save_figs = args.save_figs
rm_bad_reco = args.rm_bad_reco
zee_only = args.zee_only
lh_cut_name = args.lh_cut_name
mask_path = args.mask_path
verbose = args.verbose
input_json = args.input_json
output_json = args.output_json

if args.wandb:
    import wandb as wb
    wb.init(sync_tensorboard=True)

# Set which GPU(s) to use
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

# ==============================================================================
# Hyperparameters
# ==============================================================================
# Import parameter configurations that should be different from the default as a module
from importlib import import_module
param_conf = import_module(exp_dir.split('/')[-2] + '.param_conf')
params = param_conf.get_params()
# Use the chosen parameters where given, and use the default parameters otherwise
params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

# Update with the iDDS parameter
with open(input_json, 'r') as fp:
    idds_params = json.load(fp)
    params = dpcal.utils.merge_dicts(params, idds_params, in_depth=True)

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    params = dpcal.utils.set_auto_lr(params)

# Integrate the hyperparameters given from the command line
params['n_gpus'] = len(gpu_ids.replace(',',''))

# ==============================================================================
# Logging directories
# ==============================================================================
# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = dpcal.utils.create_directories(exp_dir, params['epochs'], log_prefix=gpu_ids)

# ==============================================================================
# Data
# ==============================================================================
# Load the first 1000 points, except if something else was given from the
# command line. To load all points, set to None.
n_points = {'train':None, 'val':None, 'test':None}
for set_name, n in zip(n_points, [_n_train, _n_val, _n_test]):
    if not n < 0:
        n_points[set_name] = n

# If a datagenerator is used, load a single point for each set (e.g. train, val
# and test) to construct the model with (the construction uses the shapes of the
# data). The actually used number of points from each set is given by
# params['data_generator']['n_points']
if params['data_generator']['use']:
    params['data_generator']['n_points'] = n_points
    n_points = {set_name:1 for set_name in n_points}
    if data_path is not None:
        params['data_generator']['path'] = data_path

# Load data
if params['data_generator']['use']:
    data = dpcal.utils.load_atlas_data(n_points=n_points,
                                       **params['data_generator']['load_kwargs'],
                                       verbose=False)
else:
    # Load data parameters
    data_conf = import_module(exp_dir.split('/')[-2] + '.data_conf')
    data_params = data_conf.get_params()

    # Get standardized versions
    if data_params['scalar_names'] is not None:
        for i,name in enumerate(data_params['scalar_names']):
            if name not in ['p_nTracks', 'p_pt_track', 'p_f0Cluster', 'p_R12']:
                data_params['scalar_names'][i] = data_params['scalar_names'][i] + '_scaled'

    # Get standardized versions
    if data_params['track_names'] is not None:
        for i,name in enumerate(data_params['track_names']):
            data_params['track_names'][i] = data_params['track_names'][i] + '_scaled'

    # Load an already existing mask, and load only the points satisfying it
    if mask_path is not None:
        # Load mask
        with open(mask_path, 'rb') as f:
            mask = joblib.load(f)

        for set_name in mask:
            if set_name!='test': # Don't mask the test set!
                n_points[set_name] = np.where(mask[set_name])[0] # A tuple, for some reason

    # Load the data
    data = dpcal.utils.load_atlas_data(path=data_path, n_points=n_points, **data_params)

    # Scale some variables here, because it wasn't done in the data production
    def scale_according_to_name(name, scaler_name='Robust'):
        if name in data_params['scalar_names']:
            n_s = data_params['scalar_names'].index(name)
            assert(data_params['scalar_names'][n_s] == name)
            print(f'Scaling {name} with {scaler_name}Scaler')
            if scaler_name.lower()=='robust':
                scaler = RobustScaler(copy=False)
            elif scaler_name.lower()=='quantile':
                scaler = QuantileTransformer(output_distribution='normal', copy=False)
            scaler.fit(np.expand_dims(data['train']['scalars'][:,n_s],1))
            for set_name in data:
                data[set_name]['scalars'][:,n_s] = np.squeeze(scaler.transform(np.expand_dims(data[set_name]['scalars'][:,n_s],1)))
            # Save scaler
            with open(dirs['log'] + f'scaler_{name}.jbl', 'wb+') as f:
                joblib.dump(scaler, f)

    if data_params['scalar_names'] is not None:
        # Loop over unscaled scalars
        for name in ['p_nTracks', 'p_pt_track', 'p_f0Cluster', 'p_R12']:
            scale_according_to_name(name, scaler_name=('Quantile' if name=='p_pt_track' else 'Robust'))

# ==============================================================================
# Apply masks
# ==============================================================================
if not params['data_generator']['use'] and mask_path is None:

    if rm_bad_reco or zee_only or lh_cut_name:
        # Make masks
        mask = dpcal.utils.make_mask(data_path, n_points, rm_bad_reco=rm_bad_reco,
                                      zee_only=zee_only, lh_cut_name=lh_cut_name)

        # Apply mask
        data = dpcal.utils.apply_mask(data, mask, skip_name='test') # Don't apply to the test set

        # Save mask
        with open(dirs['log'] + 'mask.jbl', 'wb+') as f:
            joblib.dump(mask, f)

# ==============================================================================
# Create and use model
# ==============================================================================
# Instantiate model container (with self.model in it)
mc = dpcal.ModelContainer(data=data,
                          params=params,
                          dirs=dirs,
                          save_figs=save_figs,
                          verbose=verbose)

# Save hyperparameters (with some additional information)
params_for_saving = copy.deepcopy(mc.params)
params_for_saving['n_params'] = mc.model.count_params()
dpcal.utils.save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

# Save data parameters (with some additional information)
data_params['data_path'] = data_path
dpcal.utils.save_dict(data_params, dirs['log'] + 'dataparams.txt', save_pkl=True)

# Train model
mc.train()

# Evaluate (predicting and evaluating on test or validation set)
if not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results
if verbose:
    print('Evaluation scores:')
    print(mc.evaluation_scores)

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

with open(output_json, 'w') as f:
    json.dump(mc.evaluation_scores, f, cls=MyEncoder)
