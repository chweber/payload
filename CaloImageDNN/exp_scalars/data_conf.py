def get_params():
    """
    Returns a dictionary containing parameters to be passed to the loading
    function.
    """

    params = {'target_name'            : 'p_truth_E',
              'img_names'              : ['em_endcap'], # Crack: ['em_barrel', 'em_endcap', 'tile_gap'], Endcap: ['em_endcap']
              'gate_img_prefix'       : None,
              'scalar_names'           : [# 'p_nTracks', # Set to None if you only want to use the images
                                          'p_pt_track',
                                          # 'p_f0Cluster',
                                          # 'p_R12',
                                          'p_eAccCluster',
                                          'p_cellIndexCluster'],
                                          # 'p_eta',
                                          # 'p_deltaPhiRescaled2',
                                          # 'p_etaModCalo',
                                          # 'p_deltaEta2',
                                          # 'NvtxReco',
                                          # 'averageInteractionsPerCrossing',
                                          # 'p_poscs2',
                                          # 'p_dPhiTH3'],
              'track_names'            : None,
                                         # ['tracks_pt',
                                         # 'tracks_eta',
                                         # 'tracks_phi',
                                         # 'tracks_chi2',
                                         # 'tracks_pixhits',
                                         # 'tracks_scthits',
                                         # 'tracks_trthits',
                                         # 'tracks_vertex',
                                         # 'tracks_z0',
                                         # 'tracks_d0',
                                         # 'tracks_dR',
                                         # 'tracks_theta']
              'max_tracks'             : None,
              'sample_weight_name'     : None
              }

    return params
