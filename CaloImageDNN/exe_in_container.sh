# Set $CURRENT_DIR to /srv/workDir in case you need to come back (in this example you are in $CURRENT_DIR all the time)
export CURRENT_DIR=$PWD

# Set $CALO_DNN_DIR to payload for convenient
export CALO_DNN_DIR=/ATLASMLHPO/payload/CaloImageDNN

# Extend $PYTHONPATH to be able to import modules
export PYTHONPATH=$PYTHONPATH:$CALO_DNN_DIR/deepcalo:/ATLASMLHPO/module

# Download training dataset (to $CURRENT_DIR since you did not nevigate away)
curl -sSL https://cernbox.cern.ch/index.php/s/HfHYEsmJNWiefu3/download | tar -xzvf -;

# Coinvert input point
python $CALO_DNN_DIR/scripts/make_input.py input.json input_new.json

# Copy a folder that will hold outpout log files to $CURRENT_DIR since everywhere is read-only except for /srv (this can be avoided with a better design)
cp -r $CALO_DNN_DIR/exp_scalars $CURRENT_DIR/

# Run training, note input_new.json used instead of input.json; output.json will be produced under $CURRENT_DIR 
python /ATLASMLHPO/payload/CaloImageDNN/run_model.py -i input_new.json --exp_dir $CURRENT_DIR/exp_scalars/ --data_path $CURRENT_DIR/dataset/event100.h5 --rm_bad_reco True --zee_only True -g 0

# Remove the unused exp_scalars/
rm -fr $CURRENT_DIR/exp_scalars/

# tar mlruns output to upload to Panda
tar cvfz $CURRENT_DIR/metrics.tgz mlruns/*
