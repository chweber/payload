#!/bin/bash

## linux packages
apt-get -y install libopenblas-dev
apt-get -y install libomp-dev

## python packages
# CUDA=cu101
CUDA=cpu
TORCH=1.6.0
pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
pip install pytorch-lightning==1.0.2
pip install torch-geometric
pip install graph_nets
pip install faiss
pip install mpi4py

## install payload (use a special branch)
cd exatrkx-iml2020
# git checkout --track remotes/origin/v1.1.1
pip install .
cd ..
export TRKXINPUTDIR="./data/train_10evts"
export TRKXOUTPUTDIR="./output"

## download input data
mkdir data && cd data
#wget https://portal.nersc.gov/project/atlas/xju/train_10evts.tar
wget https://cernbox.cern.ch/index.php/s/rUPUgJ5exEYZOX6/download
tar xzf download train_10evts.tar
rm -f download train_10evts.tar
cd ..
