import yaml, json, os
from exatrkx import config_dict

action = 'filtering'
config_file = os.environ['payload_dir'] + f'/exatrkx-iml2020/exatrkx/configs/{config_dict[action]}'
new_config_file = f'{config_dict[action]}'

with open(config_file) as file:
    orig = fruits_list = yaml.load(file, Loader=yaml.FullLoader)

hp = json.load(open('input.json'))
orig.update(hp)

with open(new_config_file, 'w') as outfile:
    print('Create', new_config_file)
    yaml.dump(orig, outfile,  default_flow_style=False)
