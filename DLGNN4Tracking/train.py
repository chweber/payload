#!/usr/bin/env python
# Rui Zhang 8.2020
# rui.zhang@cern.ch

# Distributed learning payload
# Adapted from tutorial: https://indico.cern.ch/event/852553/timetable/#80-tracking-gnn-walk-through

import sys
import torch
import tensorflow as tf

import os
if 'TRKXINPUTDIR' not in os.environ:
    os.environ['TRKXINPUTDIR'] = "./data/train_10evts"
if 'TRKXOUTPUTDIR' not in os.environ:
    os.environ['TRKXOUTPUTDIR'] = "./output"
if 'payload_dir' not in os.environ:
    os.environ['payload_dir'] = "."

# system import
import yaml
import pprint

import pkg_resources

# 3rd party
import torch
from trackml.dataset import load_event
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint

# local import
from exatrkx import config_dict # for accessing predefined configuration files
from exatrkx import outdir_dict # for accessing predefined output directories
from exatrkx.src import utils_dir


# for preprocessing
from exatrkx import FeatureStore

# for embedding
from exatrkx import LayerlessEmbedding
from exatrkx import EmbeddingInferenceCallback
# for filtering
from exatrkx import VanillaFilter
from exatrkx import FilterInferenceCallback

''' Pre-processing '''
print('\033[92m[INFO]\033[0m', '\033[92mPre-processing\033[0m'.rjust(40, ' '))
hits, cell, particles, truth = load_event(os.environ['TRKXINPUTDIR'] + "/event000001000")
action = 'build'

config_file = os.environ['payload_dir'] + f'/exatrkx-iml2020/exatrkx/configs/{config_dict[action]}'

with open(config_file) as f:
  b_config = yaml.load(f, Loader=yaml.FullLoader)

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(b_config)

print('\033[92m[INFO]\033[0m', '\033[92mStore Features\033[0m'.rjust(40, ' '))
preprocess_dm = FeatureStore(b_config)
if 'test' not in sys.argv:
    preprocess_dm.prepare_data()
else:
    print('\033[92m[INFO]\033[0m', '\033[92mSkipped preprocess\033[0m'.rjust(40, ' '))
    

feature_data = torch.load(os.environ['TRKXOUTPUTDIR']  + "/feature_store/1000", map_location='cpu')


''' Embedding '''
print('\033[92m[INFO]\033[0m', '\033[92mEmbedding\033[0m'.rjust(40, ' '))
action = 'embedding'
config_file = os.environ['payload_dir'] + f'/exatrkx-iml2020/exatrkx/configs/{config_dict[action]}'


with open(config_file) as f:
  e_config = yaml.load(f, Loader=yaml.FullLoader)

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(e_config)

e_config['train_split'] = [1, 1, 1]

e_model = LayerlessEmbedding(e_config)

try:
    e_checkpoint_callback = ModelCheckpoint(
        monitor='val_loss',
        filepath=os.path.join(utils_dir.embedding_outdir,'ckpt-{epoch:02d}-{val_loss:.2f}') ,
        save_top_k=3,
        mode='min')
except:
    e_checkpoint_callback = ModelCheckpoint(
        monitor='val_loss',
        filename=os.path.join(utils_dir.embedding_outdir,'ckpt-{epoch:02d}-{val_loss:.2f}') ,
        save_top_k=3,
        mode='min')
e_callback_list = [EmbeddingInferenceCallback()]

ngpu = [s for s in sys.argv if 'ngpu=' in s]
ngpu = int(ngpu[0].split('=')[-1]) if ngpu else 0
ngpu = list(range(ngpu)) if list(range(ngpu)) else None
print('Use ngpu', ngpu)

e_trainer = Trainer(
    max_epochs = 2,
    limit_train_batches=1,
    limit_val_batches=1,
    callbacks=e_callback_list,
    gpus=ngpu,
    checkpoint_callback=e_checkpoint_callback
)

e_trainer.fit(e_model)

# embed_outfile = os.path.join(utils_dir.embedding_outdir, "train", "1002")
# dd = torch.load(embed_outfile)

'''
The above steps can be executed via a command line
run_lightning.py --action embedding --max_epochs 2 --gpus 0 --limit_train_batches 1 --limit_val_batches 1
However, running this script has more flexibility to run built-in run_lightning.py which doesn't find the config file easily
'''

''' Filtering '''
print('\033[92m[INFO]\033[0m', '\033[92mFiltering\033[0m'.rjust(40, ' '))
action = 'filtering'
# config_file = os.environ['payload_dir'] + f'/exatrkx-iml2020/exatrkx/configs/{config_dict[action]}'
config_file = config_dict[action] if os.path.exists(config_dict[action]) else os.environ['payload_dir'] + f'/exatrkx-iml2020/exatrkx/configs/{config_dict[action]}'
print('zhangr config_file action 3 ', config_file)

with open(config_file) as f:
  f_config = yaml.load(f, Loader=yaml.FullLoader)

pp.pprint(f_config)

f_config['train_split'] = [1,1, 1]

f_model = VanillaFilter(f_config)
f_callback_list = [FilterInferenceCallback()]

f_trainer = Trainer(
    max_epochs = 2,
    limit_train_batches=1,
    limit_val_batches=1,
    callbacks=f_callback_list,
    gpus=ngpu,
    )

f_trainer.fit(f_model)

# filter_outfile = os.path.join(utils_dir.filtering_outdir, "train", "1002")
# dd = torch.load(filter_outfile)

''' Graph Neural Network '''
print('\033[92m[INFO]\033[0m', '\033[92mConverting\033[0m'.rjust(40, ' '))
os.system('convert2tf.py --edge-name "e_radius" --truth-name "y_pid"')

print('\033[92m[INFO]\033[0m', '\033[92mTraining\033[0m'.rjust(40, ' '))
if 'distributed' in sys.argv:
    print('\033[92m[INFO]\033[0m', '\033[92mRun with Horovod\033[0m'.rjust(40, ' '))
    os.system('train_gnn_tf.py --max-epochs 5 -d')
else:
    print('\033[92m[INFO]\033[0m', '\033[92mRun without Horovod\033[0m'.rjust(40, ' '))
    os.system('train_gnn_tf.py --max-epochs 5')

print('\033[92m[INFO]\033[0m', '\033[92mEvaluation\033[0m'.rjust(40, ' '))
os.system('eval_gnn_tf.py --overwrite')

''' Track labeling '''

print('\033[92m[INFO]\033[0m', '\033[92mTrack labeling\033[0m'.rjust(40, ' '))
os.system('tracks_from_gnn.py')
