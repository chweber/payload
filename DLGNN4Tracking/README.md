## Horovod payload for Google Cloud
This is an example Graph Network training using Horovod to test on Google Cloud.

#
> This instruction is about how to run the payload within the [official Horovod CPU container](https://horovod.readthedocs.io/en/stable/docker_include.html).
#### Start Horovod CPU/GPU containers
Use one of the following commands (for CPUs and GPUs, respectively).
```
docker run -it -v $(pwd):/HPO gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:horovod-cpu /bin/bash

docker run -it -v $(pwd):/HPO gitlab-registry.cern.ch/zhangruihpc/evaluationcontainer:horovod-gpu /bin/bash
```

<details><summary>Use Horovod image</summary>
<p>
Use one of the following commands (for CPUs and GPUs, respectively).

```
docker run -it -v $(pwd):/HPO horovod/horovod:0.20.0-tf2.3.0-torch1.6.0-mxnet1.5.0-py3.7-cpu /bin/bash
docker run -it -v $(pwd):/HPO horovod/horovod:0.20.0-tf2.3.0-torch1.6.0-mxnet1.6.0.post0-py3.7-cuda10.1  /bin/bash
```

If you didn't use the gitlab registries, you need to fetch the code first.
Otherwise, you can skip this step.
```
git clone --recursive https://gitlab.cern.ch/zhangruihpc/DLGNN4Tracking.git
```
If you did so, replace `/ATLASMLHPO/payload/DLGNN4Tracking` to your correct path below.
</p>
</details>


#### Setup
This step includes three parts:
- install the payload;
- download inputs.

When running locally, use [cpu] for CPUs and [cu101] for GPUs.
The option is not needed if running iDDS HPO task via PanDA.
```
source /ATLASMLHPO/payload/DLGNN4Tracking/setup_gitlab.sh [cu101]
```

#### Run training
```
## Convert iDDSD input.json to exatrkx-iml2020/exatrkx/configs/train_filter.yaml
python /ATLASMLHPO/payload/DLGNN4Tracking/convert_input.py
python /ATLASMLHPO/payload/DLGNN4Tracking/train.py [test] [distributed] [ngpu=2]
```
- If keyword `test` is provided, `Store Features` is skipped. [`train.py`](train.py#L58).
- If keyword `distributed` is provided, `horovod` is enabled.
- For GPU payload, provide the number of GPUs to run on via `ngpu=?`.
