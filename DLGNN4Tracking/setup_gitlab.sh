#!/bin/bash

current_dir=$(pwd)
export payload_dir=/ATLASMLHPO/payload/DLGNN4Tracking/
## python packages
# CUDA=cu101 | cpu
CUDA=$1
[ "$CUDA" != "" ] && echo $CUDA
[ "$CUDA" != "" ] && pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
[ "$CUDA" != "" ] && pip install torch-scatter -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
[ "$CUDA" != "" ] && pip install torch-sparse -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
[ "$CUDA" != "" ] && pip install torch-cluster -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html
[ "$CUDA" != "" ] && pip install torch-spline-conv -f https://pytorch-geometric.com/whl/torch-1.6.0+${CUDA}.html

export TRKXINPUTDIR=${current_dir}"data"
export TRKXOUTPUTDIR=${current_dir}"output"

## download input data
mkdir -p $TRKXINPUTDIR && cd $TRKXINPUTDIR
#wget https://portal.nersc.gov/project/atlas/xju/train_10evts.tar
# tar xzf download train_10evts.tar
# rm -f download train_10evts.tar

curl -sSL https://cernbox.cern.ch/index.php/s/PKXAwYTfTM5gb6V/download | tar -xzvf -;
export TRKXINPUTDIR=${TRKXINPUTDIR}"/train_10evts"
cd ${current_dir}